import datetime
import logging
import os
import numpy
import json
import requests
import time

def log(msg):
    msg = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S") +" "+msg
    print(msg)
    if log != None: logging.info(msg)

# Process settings
log("Reading settings...")
settings = dict()
if os.path.exists("settings.json"):
    try:
        with open("settings.json","r", encoding='utf-8') as file:
            settings = json.load(file)
    except:
        log("Error reading settings file. Defaults are retained.")
        settings = dict()
else:
    log("Settings file not found. Defaults are retained.")

# Check if water
map = numpy.zeros((180,360),dtype=numpy.byte)

f = open("iswater.txt", "a")
log("Checking water...")
for i in range(0,180):
    for j in range(0,360):
        lat = float(90 - i)
        lon = float(j - 180)
        if lat>-34 or (lat==-34 and lon<=-2): continue
        url = f"https://api.onwater.io/api/v1/results/{lat},{lon}?access_token={settings['onwatertoken']}"
        r = requests.get(url, allow_redirects=True)
        log(str(r.content))
        f.write(str(r.content) + "\n")      
        f.flush()  
        raw = json.loads(r.content)
        if "water" in raw:
            if raw["water"]=="true":
                map[i,j] = 1
        time.sleep(10)
f.close()

#https://api.onwater.io/api/v1/results/79.9,-79.9?access_token=cJQas1KG4cf8aTx7spQ-