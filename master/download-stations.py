import json
import requests
import gzip
import shutil
import os
import pyodbc
import datetime
import dateutil.relativedelta

print("Downloading weather stations list...")

#https://bulk.meteostat.net/v2/stations/lite.json.gz

url = "https://bulk.meteostat.net/v2/stations/lite.json.gz"
r = requests.get(url, allow_redirects=True)
open('lite.json.gz', 'wb').write(r.content)

print("Unpacking weather stations list...")
with gzip.open('lite.json.gz', 'rb') as f_in:
    with open('lite.json', 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)

print("Connecting to the database...")
conn = pyodbc.connect(r"Driver={ODBC Driver 17 for SQL Server};Server=(localdb)\MSSQLLocalDB;Data Source=(localdb)\MSSQLLocalDB;Database=weather;Integrated Security=True;Connect Timeout=5;ApplicationIntent=ReadWrite;")
dbc = conn.cursor()
dbc.fast_executemany = True

print("Reading weather stations list...")
with open("lite.json","r",encoding="utf-8") as file:
    data = json.load(file)
    deleteParams = list()
    insertParams = list()
    for station in data:
        if station['id'] == None or station['inventory']['daily']['start'] == None or station['inventory']['daily']['end'] == None: continue
        name = list(station['name'].values())[0]
        name = name.replace("'","")[:100]
        print(f"Processing {station['id']}...")
        deleteQuery = f"DELETE FROM Stations WHERE ID=?;"
        deleteParams.append((station['id'],))
        insertQuery = f"INSERT INTO Stations VALUES (?, ?, ?, ?, ?, ?,CONVERT(date,?), CONVERT(date,?));"
        insertParams.append((station['id'], name, station['country'],station['location']['latitude'],station['location']['longitude'],
            station['location']['elevation'],station['inventory']['daily']['start'], station['inventory']['daily']['end']))
    print("Committing to the database...")
    dbc.executemany(deleteQuery, deleteParams)        
    dbc.executemany(insertQuery, insertParams)
    dbc.commit()
    print("Committed to the database.")

# Remove all stations with last data point older than 1 month
print("Cleaning up inactive stations list...")
cutoffDate = datetime.datetime.strftime(datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=+1),"%Y-%m-%d")
selectQuery = f"SELECT ID FROM Stations WHERE EndDate < CONVERT(date,?);"
selectParams = cutoffDate
dbc.execute(selectQuery, selectParams)
results = dbc.fetchall()
inactiveStations = list()
deleteParams = list()
for r in results:
    inactiveStations.append(r[0])
for s in inactiveStations:
    deleteQuery = f"DELETE FROM Stations WHERE ID=?;"
    deleteParams.append((s,))
dbc.executemany(deleteQuery,deleteParams)
dbc.commit()

print("Deleting temporary files...")
if os.path.exists("lite.json.gz"): os.remove("lite.json.gz")
if os.path.exists("lite.json"): os.remove("lite.json")

print("Closing the database...")
dbc.close()
conn.close()

print("Success")