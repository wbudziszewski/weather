from asyncio.windows_events import NULL
import os
import datetime
import dateutil
import logging
import sys
import re
import json
import smtplib
#import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import matplotlib.pyplot as plot
import io
import pyodbc
import numpy

def log(msg):
    msg = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S") +" "+msg
    print(msg)
    if log != None: logging.info(msg)

def getCoords(latitude, longitude, link):
    retval = ""
    if link: retval = f"<a href=\"https://www.google.com/maps/@{str(latitude)},{str(longitude)},8z\">"
    if latitude > 0:
        retval = retval + str.format("{:.2f}",latitude) + " °N, "
    else:
        retval = retval + str.format("{:.2f}",-latitude) + " °S, "    
    if longitude > 0:
        retval = retval + str.format("{:.2f}",longitude) + " °W"
    else:
        retval = retval + str.format("{:.2f}",-longitude) + " °E"  
    if link: retval = retval + "</a>"
    return retval 

def getWebColor(temp: float, scale: dict) -> str:
    if temp < list(scale.keys())[0]: temp = list(scale.keys())[0]
    if temp > list(scale.keys())[len(scale.keys())-1]: temp = list(scale.keys())[len(scale.keys)-1]
    color = scale[round(temp,0)]
    return format(color[0], "x").ljust(2,"0")+format(color[1], "x").ljust(2,"0")+format(color[2], "x").ljust(2,"0")

args = sys.argv[1:]
singleReceiver = False # Data will be sent only to the first designated receiver
for a in args:
    if a.lower() == "--single": singleReceiver = True

args = sys.argv[1:]
logFile = None
for a in args:
    s = re.search("--log=(.+)", a)
    if s and len(s.groups())>0:
        logFile = s.group(1)
        if os.path.exists(logFile): os.remove(logFile)
        logging.basicConfig(filename=logFile, encoding='utf-8', level=logging.DEBUG)

tempscale = {
        -50: [0, 0, 255], -49: [0,10, 255], -48: [0,20, 255], -47: [0,30, 255], -46: [0,40, 255], -45: [0,50, 255], -44: [0,60, 255], -43: [0,70, 255], -42: [0,80, 255], -41: [0,90, 255],
        -40: [0,100,255], -39: [0,110,255], -38: [0,120,255], -37: [0,130,255], -36: [0,140,255], -35: [0,150,255], -34: [0,160,255], -33: [0,170,255], -32: [0,180,255], -31: [0,190,255],
        -30: [0,200,255], -29: [0,210,255], -28: [0,220,255], -27: [0,230,255], -26: [0,240,255], -25: [0,255,255], -24: [0,255,240], -23: [0,255,230], -22: [0,255,220], -21: [0,255,210],
        -20: [0,255,200], -19: [0,255,190], -18: [0,255,180], -17: [0,255,170], -16: [0,255,160], -15: [0,255,150], -14: [0,255,140], -13: [0,255,130], -12: [0,255,120], -11: [0,255,110],
        -10: [0,255,100],  -9: [0,255,90 ],  -8: [0,255,80 ],  -7: [0,255,70 ],  -6: [0,255,60 ],  -5: [0,255,50 ],  -4: [0,255,40 ],  -3: [0,255,30 ],  -2: [0,255,20 ],  -1: [0,255,10 ],
        0: [0,255,0  ],   1: [10,255,0 ],   2: [20,255,0 ],   3: [30,255,0 ],   4: [40,255,0 ],   5: [50,255,0 ],   6: [60,255,0 ],   7: [70,255,0 ],   8: [80,255,0 ],   9: [90,255,0 ],
        10: [100,255,0],  11: [110,255,0],  12: [120,255,0],  13: [130,255,0],  14: [140,255,0],  15: [150,255,0],  16: [160,255,0],  17: [170,255,0],  18: [180,255,0],  19: [190,255,0],
        20: [200,255,0],  21: [210,255,0],  22: [220,255,0],  23: [230,255,0],  24: [240,255,0],  25: [255,255,0],  26: [255,240,0],  27: [255,230,0],  28: [255,220,0],  29: [255,210,0],
        30: [255,200,0],  31: [255,190,0],  32: [255,180,0],  33: [255,170,0],  34: [255,160,0],  35: [255,150,0],  36: [255,140,0],  37: [255,130,0],  38: [255,120,0],  39: [255,110,0],
        40: [255,100,0],  41: [255,90,0 ],  42: [255,80,0 ],  43: [255,70,0 ],  44: [255,60,0 ],  45: [255,50,0 ],  46: [255,40,0 ],  47: [255,30,0 ],  48: [255,20,0 ],  49: [255,10,0 ],
        50: [255,0,0  ]
        }

# Process settings
log("Reading settings...")
settings = dict()
if os.path.exists("settings.json"):
    try:
        with open("settings.json","r", encoding='utf-8') as file:
            settings = json.load(file)
    except:
        log("Error reading settings file. Defaults are retained.")
        settings = dict()
else:
    log("Settings file not found. Defaults are retained.")

# Get database data.
log("Connecting to the database...")
conn = pyodbc.connect(r"Driver={ODBC Driver 17 for SQL Server};Server=(localDB)\MSSQLLocalDB;Data Source=(localdb)\MSSQLLocalDB;Database=weather;Integrated Security=True;Connect Timeout=5;ApplicationIntent=ReadWrite;")
dbc = conn.cursor()
localdata = list()
localprec = list()

# Local station data
log("Querying local station data...")
now = datetime.datetime.now().date()
currentDate = datetime.datetime.strftime(now,"%Y-%m-%d")
futureDate = datetime.datetime.strftime(datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1),"%Y-%m-%d")
pastDate = datetime.datetime.strftime(datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=3),"%Y-%m-%d")
selectQuery = f"SELECT Date, GenDate, TempAvg, TempMin, TempMax, Precipitation FROM Data WHERE ID = ? AND Date >= CONVERT(date,?) AND Date <= CONVERT(date,?) ORDER BY Date ASC"
selectParams = (settings['localstation'],pastDate,futureDate)
dbc.execute(selectQuery, selectParams)
results = dbc.fetchall()
for row in results:
    localdata.append({
        "date":         row[0],
        "genDate":      row[1],
        "tempAvg":      row[2],
        "tempMin":      row[3],
        "tempMax":      row[4],
        "precipitation":row[5]
        })

log("Querying local station precipitation...")
selectQuery = f"SELECT Date, Precipitation FROM Data WHERE ID = ? AND YEAR(Date) = ?"
selectParams = (settings['localstation'],now.year-1)
dbc.execute(selectQuery, selectParams)
results = dbc.fetchall()
for row in results:
    localprec.append({
        "date":         row[0],
        "precipitation":row[1]
        })
localprecMax = {
        "date":         now,
        "precipitation":0
        }
for n in localprec:
    if n["precipitation"] != None and n["precipitation"] > localprecMax["precipitation"]: localprecMax = n

# Records
log("Querying record temperatures...")
latBounds = [90, 60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -90]
latBoundsStr = ["90°N","60°N","50°N","40°N","30°N","20°N","10°N","0°","10°S","20°S","30°S","40°S","90°S"]
latMax = list()
latMin = list()
for i in range(len(latBounds)-1):
    selectQuery = (f"SELECT TOP 1 Stations.Name, Countries.Name, Stations.Latitude, Stations.Longitude, Stations.Elevation, Data.TempAvg, Data.TempMin, Data.TempMax FROM Data "
    f"INNER JOIN Stations ON Data.ID = Stations.ID "
    f"INNER JOIN Countries ON Stations.Country = Countries.Alpha2Code "
    f"WHERE Stations.Latitude <= ? AND Stations.Latitude > ? AND Data.Date = CONVERT(date,?) "
    f"ORDER BY Data.TempMax DESC;" ) 
    selectParams = (latBounds[i], latBounds[i+1], currentDate)
    dbc.execute(selectQuery, selectParams)
    results = dbc.fetchall()
    if len(results) > 0:
        latMax.append({
            "station":      results[0][0],
            "country":      results[0][1],
            "latitude":     results[0][2],
            "longitude":    results[0][3],
            "elevation":    results[0][4],
            "tempAvg":      results[0][5],
            "tempMin":      results[0][6],
            "tempMax":      results[0][7]
            })

    selectQuery = (f"SELECT TOP (1) Stations.Name, Countries.Name, Stations.Latitude, Stations.Longitude, Stations.Elevation, Data.TempAvg, Data.TempMin, Data.TempMax FROM Data "
    f"INNER JOIN Stations ON Data.ID = Stations.ID "
    f"INNER JOIN Countries ON Stations.Country = Countries.Alpha2Code "
    f"WHERE Stations.Latitude <= ? AND Stations.Latitude > ? AND Data.Date = CONVERT(date,?) "
    f"ORDER BY Data.TempMin ASC;") 
    selectParams = (latBounds[i], latBounds[i+1], currentDate)
    dbc.execute(selectQuery, selectParams)
    results = dbc.fetchall()
    if len(results) > 0:
        latMin.append({
            "station":      results[0][0],
            "country":      results[0][1],
            "latitude":     results[0][2],
            "longitude":    results[0][3],
            "elevation":    results[0][4],
            "tempAvg":      results[0][5],
            "tempMin":      results[0][6],
            "tempMax":      results[0][7]
            })

totMax = { "tempMax": -999 }
totMin = { "tempMin": 999 }
for r in latMax:
    if r["tempMax"] > totMax["tempMax"]: totMax = r
for r in latMin:
    if r["tempMin"] < totMax["tempMin"]: totMin = r

log("Querying record precipitation...")
precMax = {}
selectQuery = (f"SELECT TOP (1) Stations.Name, Countries.Name, Stations.Latitude, Stations.Longitude, Stations.Elevation, Data.Precipitation FROM Data "
    f"INNER JOIN Stations ON Data.ID = Stations.ID "
    f"INNER JOIN Countries ON Stations.Country = Countries.Alpha2Code "
    f"WHERE Data.Date = CONVERT(date,?) AND Data.Precipitation IS NOT NULL ORDER BY Data.Precipitation DESC;") 
selectParams = (currentDate)
dbc.execute(selectQuery, selectParams)
results = dbc.fetchall()
if len(results) > 0:
    precMax = {
        "station":      results[0][0],
        "country":      results[0][1],
        "latitude":     results[0][2],
        "longitude":    results[0][3],
        "elevation":    results[0][4],
        "precipitation":results[0][5]
        }

# Local station recent temperature relative to the history
log("Checking local station temperature relative to the history...")
relative = {}
# Get data
selectQuery = f"SELECT T.Date, T.TempAvg FROM (SELECT ID, Date, TempAvg FROM Data UNION SELECT ID, Date, TempAvg FROM Archive) T WHERE T.ID = ? AND T.Date <= CONVERT(date,?)"
selectParams = (settings['localstation'], currentDate)
dbc.execute(selectQuery, selectParams)
results = dbc.fetchall()
relativeSource = list()
for row in results:
    relativeSource.append({
        "date":         row[0],
        "temp":         row[1],
        })
# Current date
relative["local/day/temp"] = None
relative["local/day/less"] = 0
relative["local/day/count"] = 0
relative["local/day/avg"] = 0
for r in relativeSource:
    if r["date"] == now:
        relative["local/day/temp"] = r["temp"]
if relative["local/day/temp"] != None:
    for r in relativeSource:
        try:
            if datetime.date(year=now.year, month=r["date"].month, day=r["date"].day) == now:
                relative["local/day/avg"] = relative["local/day/avg"] + r["temp"]
                relative["local/day/count"] = relative["local/day/count"] + 1
                if r["temp"] < relative["local/day/temp"]:
                    relative["local/day/less"] = relative["local/day/less"] + 1
        except ValueError:
            pass
if relative["local/day/count"] > 0:
    relative["local/day/avg"] = relative["local/day/avg"] / relative["local/day/count"]
else:
    relative["local/day/avg"] = None
for r in relativeSource:
    if r["date"].month == now.month and r["date"].day == now.day:
        relative["local/day/"+str(r["date"].year)] = r["temp"]

# Last week
relative["local/week/temp"] = None
relative["local/week/less"] = 0
relative["local/week/count"] = 0
relative["local/week/avg"] = 0
sum = {}
count = {}
avg = {}
for r in relativeSource:
    if r["date"] <= now and r["date"] > (now - dateutil.relativedelta.relativedelta(days=7)):
        sum[now.year] = sum[now.year] + r["temp"] if now.year in sum else r["temp"]
        count[now.year] = count[now.year] + 1 if now.year in count else 1
if now.year in sum and now.year in count: 
    relative["local/week/temp"] = sum[now.year] / count[now.year]
sum.clear()
count.clear()
if relative["local/week/temp"] != None:
    for r in relativeSource:
        try:
            calcdate = datetime.date(year=now.year, month=r["date"].month, day=r["date"].day)
            calcyear = r["date"].year + (1 if (r["date"] + dateutil.relativedelta.relativedelta(days=7)).year != r["date"].year else 0)
            if calcdate <= now and calcdate > now - dateutil.relativedelta.relativedelta(days=7):
                sum[calcyear] = sum[calcyear] + r["temp"] if calcyear in sum else r["temp"]
                count[calcyear] = count[calcyear] + 1 if calcyear in count else 1
        except ValueError:
            pass
    for key in sum:
        avg[key] = sum[key] / count[key]
    sum.clear()
    count.clear()
    for key in avg:
        relative["local/week/avg"] = relative["local/week/avg"] + avg[key]
        relative["local/week/count"] = relative["local/week/count"] + 1
        if avg[key] < relative["local/week/temp"]:
            relative["local/week/less"] = relative["local/week/less"] + 1
    relative["local/week/avg"] = relative["local/week/avg"] / len(avg)
    for key in avg:
        relative["local/week/"+str(key)] = avg[key]

# Last month
relative["local/month/temp"] = None
relative["local/month/less"] = 0
relative["local/month/count"] = 0
relative["local/month/avg"] = 0
sum = {}
count = {}
avg = {}
for r in relativeSource:
    if r["date"] <= now and r["date"] > (now - dateutil.relativedelta.relativedelta(months=1)):
        sum[now.year] = sum[now.year] + r["temp"] if now.year in sum else r["temp"]
        count[now.year] = count[now.year] + 1 if now.year in count else 1
if now.year in sum and now.year in count: 
    relative["local/month/temp"] = sum[now.year] / count[now.year]
sum.clear()
count.clear()
if relative["local/month/temp"] != None:
    for r in relativeSource:
        try:
            calcdate = datetime.date(year=now.year, month=r["date"].month, day=r["date"].day)
            calcyear = r["date"].year + (1 if (r["date"] + dateutil.relativedelta.relativedelta(months=1)).year != r["date"].year else 0)
            if calcdate <= now and calcdate > now - dateutil.relativedelta.relativedelta(months=1):
                sum[calcyear] = sum[calcyear] + r["temp"] if calcyear in sum else r["temp"]
                count[calcyear] = count[calcyear] + 1 if calcyear in count else 1
        except ValueError:
            pass
    for key in sum:
        avg[key] = sum[key] / count[key]
    sum.clear()
    count.clear()
    for key in avg:
        relative["local/month/avg"] = relative["local/month/avg"] + avg[key]
        relative["local/month/count"] = relative["local/month/count"] + 1
        if avg[key] < relative["local/month/temp"]:
            relative["local/month/less"] = relative["local/month/less"] + 1
    relative["local/month/avg"] = relative["local/month/avg"] / len(avg)
    for key in avg:
        relative["local/month/"+str(key)] = avg[key]

# Prepare the map data
log("Querying temperature map data...")
selectQuery = f"SELECT Data.TempAvg, Data.TempMin, Data.TempMax, Stations.Latitude, Stations.Longitude FROM Data INNER JOIN Stations ON Data.ID = Stations.ID WHERE Data.Date = CONVERT(date,?) ORDER BY Stations.Latitude DESC;"
selectParams = (currentDate)
dbc.execute(selectQuery, selectParams)
results = dbc.fetchall()
mapsrc = list()
for row in results:
    temp = row[0]
    if temp==None: temp = (row[1]+row[2])/2
    temp = max(temp,-50)
    temp = min(temp,50)
    mapsrc.append([row[3],row[4], temp])

dbc.close()
conn.close()

# Prepare email message.
log("Preparing email message...")
msg = MIMEMultipart("alternative")
receiverCounter = 0
for a in settings["toaddresses"]:
    if receiverCounter>0 and singleReceiver: continue
    receiverCounter = receiverCounter + 1
    msg["subject"] = settings["subject"] + " " + datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d')
    msg["from"] = settings["fromaddress"]
    msg["to"] = a
    message = ""
    message = message + "<html><body>"
    message = message + f"{settings['greetings']} {a.split(' ')[0].strip()}"
    message = message + f"<p>Najwyższa temperatura jest w miejscowości {totMax['station']} ({totMax['country']}, {getCoords(totMax['latitude'],totMax['longitude'],True)}): {totMax['tempMax']} °C</p>"
    message = message + f"<p>Najniższa temperatura jest w miejscowości {totMin['station']} ({totMin['country']}, {getCoords(totMin['latitude'],totMin['longitude'],True)}): {totMin['tempMin']} °C</p>"
    message = message + f"<p>Najwyższe temperatury według szerokości geograficznej:</p>"
    message = message + f"<p><table style='border-spacing: 0; border: 1px solid black;'><tbody><tr style='background-color:#A0A0A0; font-weight: bold; border: 1px solid black;'><td style='border: 1px solid black;' width='40'>Od</td><td style='border: 1px solid black;' width='40'>Do</td><td style='border: 1px solid black;' width='250'>Nazwa</td><td style='border: 1px solid black;' width='250'>Lokalizacja</td><td style='border: 1px solid black;' width='80'>Wysokość n.p.m.</td><td style='border: 1px solid black;' width='80'>Temperatura</td></tr>"
    for i in range(len(latBoundsStr)-1):
        message = message + f"<tr><td style='border: 1px solid black;'>{latBoundsStr[i]}</td><td style='border: 1px solid black;'>{latBoundsStr[i+1]}</td><td style='border: 1px solid black;'>{latMax[i]['station']}</td><td style='border: 1px solid black;'>{latMax[i]['country']}, {getCoords(latMax[i]['latitude'],latMax[i]['longitude'],True)}</td><td style='border: 1px solid black;'>{latMax[i]['elevation']} m</td><td style='border: 1px solid black; background-color:#{getWebColor(latMax[i]['tempMax'], tempscale)};'>{latMax[i]['tempMax']} °C</td></tr>"
    message = message + f"</tbody></table></p>"
    message = message + f"<p>Najniższe temperatury według szerokości geograficznej:</p>"
    message = message + f"<p><table style='border-spacing: 0; border: 1px solid black;'><tbody><tr style='background-color:#A0A0A0; font-weight: bold; border: 1px solid black;'><td style='border: 1px solid black;' width='40'>Od</td><td style='border: 1px solid black;' width='40'>Do</td><td style='border: 1px solid black;' width='250'>Nazwa</td><td style='border: 1px solid black;' width='250'>Lokalizacja</td><td style='border: 1px solid black;' width='80'>Wysokość n.p.m.</td><td style='border: 1px solid black;' width='80'>Temperatura</td></tr>"
    for i in range(len(latBoundsStr)-1):
        message = message + f"<tr><td style='border: 1px solid black;'>{latBoundsStr[i]}</td><td style='border: 1px solid black;'>{latBoundsStr[i+1]}</td><td style='border: 1px solid black;'>{latMin[i]['station']}</td><td style='border: 1px solid black;'>{latMin[i]['country']}, {getCoords(latMin[i]['latitude'],latMin[i]['longitude'],True)}</td><td style='border: 1px solid black;'>{latMin[i]['elevation']} m</td><td style='border: 1px solid black; background-color:#{getWebColor(latMin[i]['tempMin'], tempscale)};'>{latMin[i]['tempMin']} °C</td></tr>"
    message = message + f"</tbody></table></p>"
    message = message + f"<p>Najwyższe opady są w miejscowości {precMax['station']} ({precMax['country']}, {getCoords(precMax['latitude'],precMax['longitude'],True)}): {precMax['precipitation']} mm</p>"

    message = message + f"<p>Dla porównania, w Gdańsku najwyższe opady w ubiegłym roku ({datetime.datetime.strftime(localprecMax['date'],'%Y-%m-%d')}) wyniosły: {localprecMax['precipitation']} mm</p>"

    message = message + f"<h2>Temperatura w Gdańsku ({getCoords(54,18,True)}) w ostatnich trzech miesiącach + prognoza</h2>"

    # Plot the graph
    log("Plotting local station temperature graph...")
    graphX = list()
    graphYminR = list()
    graphYavgR = list()
    graphYmaxR = list()
    graphYminF = list()
    graphYavgF = list()
    graphYmaxF = list()
    ticksData = list()
    ticksLabels = list()
    for i in range(0,len(localdata)):
        graphX.append(datetime.datetime.strftime(localdata[i]["date"],"%d.%m"))
        if len(localdata) > 30:
            if i % 10 == 0:
                ticksData.append(datetime.datetime.strftime(localdata[i]["date"],"%d.%m"))
                ticksLabels.append(datetime.datetime.strftime(localdata[i]["date"],"%d.%m"))
        else:
            ticksData.append(datetime.datetime.strftime(localdata[i]["date"],"%d.%m"))
            ticksLabels.append(datetime.datetime.strftime(localdata[i]["date"],"%d.%m"))
        if localdata[i]["date"] <= localdata[i]["genDate"] and ((i < len(localdata)-1 and localdata[i+1]["date"] <= localdata[i]["genDate"]) or i == len(localdata)-1):
            graphYminR.append(localdata[i]["tempMin"])
            graphYavgR.append(localdata[i]["tempAvg"])
            graphYmaxR.append(localdata[i]["tempMax"])
            graphYminF.append(None)
            graphYavgF.append(None)
            graphYmaxF.append(None)
        elif localdata[i]["date"] <= localdata[i]["genDate"] and (i < len(localdata)-1 and localdata[i+1]["date"] > localdata[i]["genDate"]):
            graphYminR.append(localdata[i]["tempMin"])
            graphYavgR.append(localdata[i]["tempAvg"])
            graphYmaxR.append(localdata[i]["tempMax"])
            graphYminF.append(localdata[i]["tempMin"])
            graphYavgF.append(localdata[i]["tempAvg"])
            graphYmaxF.append(localdata[i]["tempMax"])
        else:
            graphYminR.append(None)
            graphYavgR.append(None)
            graphYmaxR.append(None)
            graphYminF.append(localdata[i]["tempMin"])
            graphYavgF.append(localdata[i]["tempAvg"])
            graphYmaxF.append(localdata[i]["tempMax"])

    plot.plot(graphX, graphYminF, color="#3CD7FF", linestyle="dashed")
    plot.plot(graphX, graphYavgF, color="#000000", linestyle="dashed")
    plot.plot(graphX, graphYmaxF, color="#CD3C00", linestyle="dashed")
    plot.plot(graphX, graphYminR, color="#3CD7FF", linestyle="solid")
    plot.plot(graphX, graphYavgR, color="#000000", linestyle="solid")
    plot.plot(graphX, graphYmaxR, color="#CD3C00", linestyle="solid")
    plot.xlabel('Data') 
    plot.xticks(ticks=ticksData, labels=ticksLabels, rotation=90)
    plot.ylabel('Temperatura')
    plot.grid(visible=True, which='both', axis='both')
    plot.title(f"Lokalna temperatura dla Gdańska w okresie {datetime.datetime.strftime(localdata[0]['date'],'%Y-%m-%d')} - {datetime.datetime.strftime(localdata[len(localdata)-1]['date'],'%Y-%m-%d')}")

    # Save the graph
    buffer = io.BytesIO()
    plot.savefig(buffer,format="png")
    buffer.seek(0)

    attachment1 = MIMEApplication(buffer.read(),Name="plot.png")
    attachment1["Content-Type"] = "image/png"
    attachment1["Content-ID"] = "<plot>"
    attachment1["Content-Transfer-Encoding"] = "base64"
    attachment1['Content-Disposition'] = "attachment; filename=plot.png"
    msg.attach(attachment1)

    message = message + "<p><img src='cid:plot' /></p>"
    #message = message + f"<blockquote><p>SELECT * FROM Forecasts WHERE Name = '{settings['localstation']}' AND ForecastDate >= CONVERT(date,'{pastDate}') AND ForecastDate <= CONVERT(date,'{futureDate}') ORDER BY ForecastDate ASC;</p></blockquote>"

    message = message + "<h2>Temperatura w Gdańsku w odniesieniu do danych historycznych</h2>"
    message = message + f"<p><table style='border-spacing: 0; border: 1px solid black;'><tbody><tr style='background-color:#A0A0A0; font-weight: bold; border: 1px solid black;'><td style='border: 1px solid black;' width='250'>Pozycja</td><td style='border: 1px solid black;' width='80'>Dzień</td><td style='border: 1px solid black;' width='80'>Ostatni tydzień</td><td style='border: 1px solid black;' width='80'>Ostatni miesiąc</td></td></tr>"
    message = message + f"<tr style='border: 1px solid black;'><td style='border: 1px solid black;'>Średnia temperatura (°C)</td><td style='border: 1px solid black; background-color:#{getWebColor(relative['local/day/temp'], tempscale)};'>{relative['local/day/temp']:.2f}</td><td style='border: 1px solid black; background-color:#{getWebColor(relative['local/week/temp'], tempscale)};'>{relative['local/week/temp']:.2f}</td><td style='border: 1px solid black; background-color:#{getWebColor(relative['local/month/temp'], tempscale)};'>{relative['local/month/temp']:.2f}</td></td></tr>"
    message = message + f"<tr style='border: 1px solid black;'><td style='border: 1px solid black;'>Odchylenie względem średniej z danych historycznych (°C)</td><td style='border: 1px solid black;'>{relative['local/day/temp']-relative['local/day/avg']:.2f}</td><td style='border: 1px solid black;'>{relative['local/week/temp']-relative['local/week/avg']:.2f}</td><td style='border: 1px solid black;'>{relative['local/month/temp']-relative['local/month/avg']:.2f}</td></td></tr>"
    message = message + f"<tr style='border: 1px solid black;'><td style='border: 1px solid black;'>Kwantyl rozkładu danych historycznych (%)*</td><td style='border: 1px solid black;'>{100*relative['local/day/less']/relative['local/day/count']:.2f}</td><td style='border: 1px solid black;'>{100*relative['local/week/less']/relative['local/week/count']:.2f}</td><td style='border: 1px solid black;'>{100*relative['local/month/less']/relative['local/month/count']:.2f}</td></td></tr>"
    message = message + f"<tr style='border: 1px solid black;'><td style='border: 1px solid black;' colspan='4'>Średnie temperatury w analogicznych okresach w latach (°C):</td></tr>"
    for i in range(1,11):
        message = message + f"<tr style='border: 1px solid black;'><td style='border: 1px solid black;'>{str(now.year - i) + ' r.'}</td><td style='border: 1px solid black; background-color:#{getWebColor(relative['local/day/'+str(now.year-i)], tempscale)};'>{relative['local/day/'+str(now.year-i)]:.2f}</td><td style='border: 1px solid black; background-color:#{getWebColor(relative['local/week/'+str(now.year-i)], tempscale)};'>{relative['local/week/'+str(now.year-i)]:.2f}</td><td style='border: 1px solid black; background-color:#{getWebColor(relative['local/month/'+str(now.year-i)], tempscale)};'>{relative['local/month/'+str(now.year-i)]:.2f}</td></tr>"
    message = message + f"</tbody></table></p>"
    message = message + f"<p>*) Określa, od temperatury w ilu analogicznych historycznych okresach tegoroczna średnia temperatura jest wyższa lub równa. 0% oznacza rekordowe zimno, 100% rekordowe ciepło.</p>"

    # Add the map
    log("Building temperature map...")
    plot.close()    
    map = numpy.zeros((180,360,3),dtype=numpy.uint8)
    for i in range(0,180):
        for j in range(0,360):
            map[i,j] = [255,255,255]
        for m in mapsrc:
            map[min(179,90-round(m[0])),min(359,round(m[1]+180))] = tempscale[round(m[2])]
    
    plot.xticks((0,90,180,270,359),("179°W","90°W","0°","90°E","179°E"))
    plot.yticks((0,30,60,90,120,150,179),("90°N","60°N","30°N","0°","30°S","60°S","90°S"))
    plot.grid(visible=True, which='both', axis='both')
    plot.title('Mapa temperatury')
    plot.imshow(map)
    # Save the map
    buffer = io.BytesIO()
    plot.savefig(buffer,format="png")
    buffer.seek(0)
    attachment2 = MIMEApplication(buffer.read(),Name="map.png")
    attachment2["Content-Type"] = "image/png"
    attachment2["Content-ID"] = "<map>"
    attachment2["Content-Transfer-Encoding"] = "base64"
    attachment2['Content-Disposition'] = "attachment; filename=map.png"
    msg.attach(attachment2)

    message = message + "<h2>Dzisiejsza mapa temperatury:</h2>"
    message = message + "<p><img src='cid:map' /></p>"

    message = message + "</body></html>"
    msg.attach(MIMEText(message, "html"))

    # Send the email.
    print(f"Sending email to {a}")
    try:
        smtp = smtplib.SMTP_SSL(settings["smtpaddress"],int(settings["smtpport"]))
        smtp.ehlo()
        smtp.login(settings["smtplogin"],settings["smtppassword"])
        smtp.sendmail(settings["fromaddress"], a, msg.as_string())
        smtp.close
        log(f"Email sent successfully")
    except BaseException as ex:
        log("Email test went wrong: "+str(type(ex))+": "+str(ex))