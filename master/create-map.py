import numpy
import matplotlib.pyplot as plot
import pyodbc

map = numpy.zeros((180,360,3),dtype=numpy.uint8)

for i in range(0,180):
    for j in range(0,360):
        map[i,j] = [255,255,255]

# Get database data.
print("Connecting to the database...")
conn = pyodbc.connect(r"Driver={ODBC Driver 17 for SQL Server};Server=(localDB)\MSSQLLocalDB;Data Source=(localdb)\MSSQLLocalDB;Database=weather;Integrated Security=True;Connect Timeout=5;ApplicationIntent=ReadWrite;")
dbc = conn.cursor()

selectQuery = f"SELECT Data.TempAvg, Data.TempMin, Data.TempMax, Stations.Latitude, Stations.Longitude FROM Data INNER JOIN Stations ON Data.ID = Stations.ID WHERE Data.Date = CONVERT(date,?) ORDER BY Stations.Latitude DESC;"
selectParams = ("2021-12-31")
dbc.execute(selectQuery, selectParams)
results = dbc.fetchall()
mapsrc = list()
for row in results:
    temp = row[0]
    if temp==None: temp = (row[1]+row[2])/2
    temp = max(temp,-50)
    temp = min(temp,50)
    mapsrc.append([row[3],row[4], temp])

colorscale = {
    -50: [0, 0, 255], -49: [0,10, 255], -48: [0,20, 255], -47: [0,30, 255], -46: [0,40, 255], -45: [0,50, 255], -44: [0,60, 255], -43: [0,70, 255], -42: [0,80, 255], -41: [0,90, 255],
    -40: [0,100,255], -39: [0,110,255], -38: [0,120,255], -37: [0,130,255], -36: [0,140,255], -35: [0,150,255], -34: [0,160,255], -33: [0,170,255], -32: [0,180,255], -31: [0,190,255],
    -30: [0,200,255], -29: [0,210,255], -28: [0,220,255], -27: [0,230,255], -26: [0,240,255], -25: [0,255,255], -24: [0,255,240], -23: [0,255,230], -22: [0,255,220], -21: [0,255,210],
    -20: [0,255,200], -19: [0,255,190], -18: [0,255,180], -17: [0,255,170], -16: [0,255,160], -15: [0,255,150], -14: [0,255,140], -13: [0,255,130], -12: [0,255,120], -11: [0,255,110],
    -10: [0,255,100],  -9: [0,255,90 ],  -8: [0,255,80 ],  -7: [0,255,70 ],  -6: [0,255,60 ],  -5: [0,255,50 ],  -4: [0,255,40 ],  -3: [0,255,30 ],  -2: [0,255,20 ],  -1: [0,255,10 ],
      0: [0,255,0  ],   1: [10,255,0 ],   2: [20,255,0 ],   3: [30,255,0 ],   4: [40,255,0 ],   5: [50,255,0 ],   6: [60,255,0 ],   7: [70,255,0 ],   8: [80,255,0 ],   9: [90,255,0 ],
     10: [100,255,0],  11: [110,255,0],  12: [120,255,0],  13: [130,255,0],  14: [140,255,0],  15: [150,255,0],  16: [160,255,0],  17: [170,255,0],  18: [180,255,0],  19: [190,255,0],
     20: [200,255,0],  21: [210,255,0],  22: [220,255,0],  23: [230,255,0],  24: [240,255,0],  25: [255,255,0],  26: [255,240,0],  27: [255,230,0],  28: [255,220,0],  29: [255,210,0],
     30: [255,200,0],  31: [255,190,0],  32: [255,180,0],  33: [255,170,0],  34: [255,160,0],  35: [255,150,0],  36: [255,140,0],  37: [255,130,0],  38: [255,120,0],  39: [255,110,0],
     40: [255,100,0],  41: [255,90,0 ],  42: [255,80,0 ],  43: [255,70,0 ],  44: [255,60,0 ],  45: [255,50,0 ],  46: [255,40,0 ],  47: [255,30,0 ],  48: [255,20,0 ],  49: [255,10,0 ],
     50: [255,0,0  ]
    }

for m in mapsrc:
    map[min(179,90-round(m[0])),min(359,round(m[1]+180))] = colorscale[round(m[2])]

plot.xticks((0,90,180,270,359),("179°W","90°W","0°","90°E","179°E"))
plot.yticks((0,30,60,90,120,150,179),("90°N","60°N","30°N","0°","30°S","60°S","90°S"))
plot.grid(visible=True, which='both', axis='both')
plot.imshow(map)
plot.show()