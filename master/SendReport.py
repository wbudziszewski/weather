from datetime import datetime
from datetime import timedelta
from distutils.log import error
import smtplib
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import matplotlib.pyplot as plot
import io
import pyodbc

from Settings import *

def GetForecastFromDB(datarow):
    f = Forecast()
    f.Name = datarow[0]
    f.Latitude = datarow[1]
    f.Longitude = datarow[2]
    f.IssueDate = datarow[3]
    f.ForecastDate = datarow[4]
    f.MinTemp = datarow[5]
    f.MaxTemp = datarow[6]
    f.Weather = datarow[7]
    f.Source = datarow[8]
    return f

# Process settings
ReadSettings("settings.json")

# Get database data.
from ForecastClass import Forecast
data = list()
errorsInRow = 0
print("Requesting weather data from the database")
#conn = pyodbc.connect(r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=.\weather.accdb;")
conn = pyodbc.connect(r"Driver={ODBC Driver 17 for SQL Server};Server=(localDB)\MSSQLLocalDB;Data Source=(localdb)\MSSQLLocalDB;Database=weather;Integrated Security=True;Connect Timeout=5;ApplicationIntent=ReadWrite;")
dbc = conn.cursor()

# Last 7 days and next 7 days (max)
currentDate = datetime.strftime(datetime.now(),"%Y-%m-%d")
futureDate = datetime.strftime(datetime.now() + timedelta(days=7),"%Y-%m-%d")
pastDate = datetime.strftime(datetime.now() - timedelta(days=7),"%Y-%m-%d")
#selectQuery = (f"WITH SRC AS (SELECT Name, Latitude, Longitude, IssueDate, ForecastDate, MinTemp, MaxTemp, Weather, source, ROW_NUMBER() OVER (PARTITION BY Name ORDER BY IssueDate) AS RN "
#f"FROM Forecasts WHERE ForecastDate = CONVERT(date,'{currentDate}')) "
#f"SELECT Name, Latitude, Longitude, IssueDate, ForecastDate, MinTemp, MaxTemp, Weather, Source FROM SRC WHERE RN = 1;")
selectQuery = f"SELECT * FROM Forecasts WHERE Name = '{settings['LocalCity']}' AND ForecastDate >= CONVERT(date,'{pastDate}') AND ForecastDate <= CONVERT(date,'{futureDate}') ORDER BY ForecastDate ASC"

print(selectQuery)
dbc.execute(selectQuery)
results = dbc.fetchall()
forecasts = list()
for row in results:
    f = GetForecastFromDB(row)
    forecasts.append(f)

for i in range(len(forecasts)-1,1,-1):
    if forecasts[i-1].ForecastDate == forecasts[i].ForecastDate:
        forecasts.pop(i) # Choose only the last available forecast.

#//SELECT * FROM Forecasts WHERE Name = "Tromsoe" ORDER BY ForecastDate ASC, IssueDate DESC;
# SELECT Name, FIRST(Latitude), FIRST(Longitude), IssueDate, FIRST(ForecastDate), FIRST(MinTemp), FIRST(MaxTemp), FIRST(Weather), FIRST(Source) FROM 

# Records
#selectQuery = (f"WITH SRC AS (SELECT Name, Latitude, Longitude, IssueDate, ForecastDate, MinTemp, MaxTemp, Weather, source, ROW_NUMBER() OVER (PARTITION BY Name ORDER BY IssueDate) AS RN "
#f"FROM Forecasts WHERE ForecastDate = CONVERT(date,'{currentDate}')) "
#f"SELECT TOP 1 Name, Latitude, Longitude, IssueDate, ForecastDate, MinTemp, MaxTemp, Weather, Source FROM SRC WHERE RN = 1 ORDER BY MaxTemp DESC;")
selectQuery = f"SELECT TOP 1 * FROM Forecasts WHERE ForecastDate = CONVERT(date,'{currentDate}') ORDER BY MaxTemp DESC;"
print(selectQuery)
dbc.execute(selectQuery)
results = dbc.fetchall()
totMax = Forecast()
if len(results) > 0:
    totMax = GetForecastFromDB(results[0])

selectQuery = f"SELECT TOP 1 * FROM Forecasts WHERE ForecastDate = CONVERT(date,'{currentDate}') ORDER BY MinTemp ASC;"
print(selectQuery)
dbc.execute(selectQuery)
results = dbc.fetchall()
totMin = Forecast()
if len(results) > 0:
    totMin = GetForecastFromDB(results[0])

latBounds = [90, 60, 50, 40, 30, 20, 10, 0, -10, -20, -30, -40, -90]
latBoundsStr = ["90°N","60°N","50°N","40°N","30°N","20°N","10°N","0°","10°S","20°S","30°S","40°S","90°S"]
latMax = list()
latMin = list()
for i in range(len(latBounds)-1):
    #selectQuery = (f"WITH SRC AS (SELECT Name, Latitude, Longitude, IssueDate, ForecastDate, MinTemp, MaxTemp, Weather, source, ROW_NUMBER() OVER (PARTITION BY Name ORDER BY IssueDate) AS RN "
    #f"FROM Forecasts WHERE Latitude <= {latBounds[i]} AND Latitude > {latBounds[i+1]} AND ForecastDate = CONVERT(date,'{currentDate}')) "
    #f"SELECT TOP 1 Name, Latitude, Longitude, IssueDate, ForecastDate, MinTemp, MaxTemp, Weather, Source FROM SRC WHERE RN = 1 ORDER BY MaxTemp DESC;")
    selectQuery = f"SELECT TOP 1 * FROM Forecasts WHERE Latitude <= {latBounds[i]} AND Latitude > {latBounds[i+1]} AND ForecastDate = CONVERT(date,'{currentDate}') ORDER BY MaxTemp DESC;"
    print(selectQuery)
    dbc.execute(selectQuery)
    results = dbc.fetchall()
    if len(results) > 0:
        latMax.append(GetForecastFromDB(results[0]))
    else:
        latMax.append(Forecast())
    selectQuery = f"SELECT TOP 1 * FROM Forecasts WHERE Latitude <= {latBounds[i]} AND Latitude > {latBounds[i+1]} AND ForecastDate = CONVERT(date,'{currentDate}') ORDER BY MinTemp ASC;"
    print(selectQuery)
    dbc.execute(selectQuery)
    results = dbc.fetchall()
    if len(results) > 0:
        latMin.append(GetForecastFromDB(results[0]))
    else:
        latMin.append(Forecast())

conn.close()

# Prepare email message.
msg = MIMEMultipart("alternative")
fromAddress = settings["FromAddress"]
for a in settings["ToAddresses"]:
    toAddress = a
    msg["Subject"] = settings["Subject"] + " " + datetime.strftime(datetime.now(),'%Y-%m-%d')
    msg["From"] = fromAddress
    msg["To"] = toAddress
    message = f"{settings['Greetings']} {toAddress.split(' ')[0].strip()}"
    message = message + f"<p>Najwyższa temperatura jest w miejscowości {totMax.Name} ({totMax.GetCoordsWithLink()}): {totMax.MaxTemp} °C</p>"
    message = message + f"<blockquote><p>SELECT TOP 1 * FROM Forecasts WHERE ForecastDate = CONVERT(date,'{currentDate}') ORDER BY MaxTemp DESC;</p></blockquote>"
    message = message + f"<p>Najniższa temperatura jest w miejscowości {totMin.Name} ({totMin.GetCoordsWithLink()}): {totMin.MinTemp} °C</p>"
    message = message + f"<blockquote><p>SELECT TOP 1 * FROM Forecasts WHERE ForecastDate = CONVERT(date,'{currentDate}') ORDER BY MaxTemp DESC;</p></blockquote>"
    message = message + f"<p>Najwyższe temperatury według szerokości geograficznej:</p>"
    message = message + f"<p><table><tr><td>Od</td><td>Do</td><td>Nazwa</td><td>Lokalizacja</td><td>Temperatura</td><td>Pogoda</td></tr>"
    for i in range(len(latBoundsStr)-1):
        message = message + f"<tr><td>{latBoundsStr[i]}</td><td>{latBoundsStr[i+1]}</td><td>{latMax[i].Name}</td><td>{latMax[i].GetCoordsWithLink()}</td><td>{latMax[i].MaxTemp} °C</td><td>{latMax[i].Weather}</td></tr>"
    message = message + f"</table></p>"
    message = message + f"<blockquote><p>SELECT TOP 1 * FROM Forecasts WHERE Latitude <= x AND Latitude > y AND ForecastDate = CONVERT(date,'{currentDate}') ORDER BY MaxTemp DESC;</p></blockquote>"
    message = message + f"<p>Najniższe temperatury według szerokości geograficznej:</p>"
    message = message + f"<p><table><tr><td>Od</td><td>Do</td><td>Nazwa</td><td>Lokalizacja</td><td>Temperatura</td><td>Pogoda</td></tr>"
    for i in range(len(latBoundsStr)-1):
        message = message + f"<tr><td>{latBoundsStr[i]}</td><td>{latBoundsStr[i+1]}</td><td>{latMin[i].Name}</td><td>{latMin[i].GetCoordsWithLink()}</td><td>{latMin[i].MinTemp} °C</td><td>{latMin[i].Weather}</td></tr>"
    message = message + f"</table></p>"
    message = message + f"<blockquote><p>SELECT TOP 1 * FROM Forecasts WHERE Latitude <= x AND Latitude > y AND ForecastDate = CONVERT(date,'{currentDate}') ORDER BY MinTemp ASC;</p></blockquote>"

    # Plot the graph
    graphX = list()
    graphYminR = list()
    graphYmaxR = list()
    graphYminF = list()
    graphYmaxF = list()
    for i in range(0,len(forecasts)):
        graphX.append(datetime.strftime(forecasts[i].ForecastDate,"%d.%m"))
        if forecasts[i].ForecastDate <= datetime.now().date() and ((i < len(forecasts)-1 and forecasts[i+1].ForecastDate <= datetime.now().date()) or i == len(forecasts)-1):
            graphYminR.append(forecasts[i].MinTemp)
            graphYmaxR.append(forecasts[i].MaxTemp)
            graphYminF.append(None)
            graphYmaxF.append(None)
        elif forecasts[i].ForecastDate <= datetime.now().date() and i < len(forecasts)-1 and forecasts[i+1].ForecastDate > datetime.now().date():
            graphYminR.append(forecasts[i].MinTemp)
            graphYmaxR.append(forecasts[i].MaxTemp)
            graphYminF.append(forecasts[i].MinTemp)
            graphYmaxF.append(forecasts[i].MaxTemp)
        else:
            graphYminR.append(None)
            graphYmaxR.append(None)
            graphYminF.append(forecasts[i].MinTemp)
            graphYmaxF.append(forecasts[i].MaxTemp)

    plot.plot(graphX, graphYminF, color="#3CD7FF", linestyle="dashed")
    plot.plot(graphX, graphYmaxF, color="#CD3C00", linestyle="dashed")
    plot.plot(graphX, graphYminR, color="#3CD7FF", linestyle="solid")
    plot.plot(graphX, graphYmaxR, color="#CD3C00", linestyle="solid")
    plot.xlabel('Date')
    plot.ylabel('Temperature')
    plot.title('Min-max temperature in ' + settings["LocalCity"])

    # Save the graph
    buffer = io.BytesIO()
    plot.savefig(buffer,format="png")
    buffer.seek(0)

    attachment = MIMEApplication(buffer.read(),Name="plot.png")
    # After the file is closed
    attachment["Content-Type"] = "image/png"
    attachment["Content-ID"] = "<plot>"
    attachment["Content-Transfer-Encoding"] = "base64"
    attachment['Content-Disposition'] = "attachment; filename=plot.png"
    msg.attach(attachment)

    message = message + "<p><img src='cid:plot' /></p>"
    message = message + f"<blockquote><p>SELECT * FROM Forecasts WHERE Name = '{settings['LocalCity']}' AND ForecastDate >= CONVERT(date,'{pastDate}') AND ForecastDate <= CONVERT(date,'{futureDate}') ORDER BY ForecastDate ASC;</p></blockquote>"

    # Save settings
    WriteSettings("settings.json")

    msg.attach(MIMEText(message, "html"))

    # Send the email.
    print(f"Sending email to {a}")
    try:
        smtp = smtplib.SMTP_SSL(settings["SMTPServerAddress"],int(settings["SMTPServerPort"]))
        smtp.ehlo()
        smtp.login(settings["SMTPLogin"],settings["SMTPPassword"])
        smtp.sendmail(fromAddress,toAddress,msg.as_string())
        smtp.close
        print(f"Email sent successfully")
    except BaseException as ex:
        print("Email test went wrong: "+str(type(ex))+": "+str(ex))