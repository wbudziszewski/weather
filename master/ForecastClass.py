from datetime import datetime

class Forecast:
    Source = ""
    Name = ""
    Latitude = 0.0
    Longitude = 0.0
    IssueDate = datetime(1,1,1)
    ForecastDate = datetime(1,1,1)
    MinTemp = 0
    MaxTemp = 0
    Weather = ""

    def GetCoords(self):
        retval = ""
        if self.Latitude > 0:
            retval = str.format("{:.2f}",self.Latitude) + " °N, "
        else:
            retval = str.format("{:.2f}",-self.Latitude) + " °S, "    
        if self.Longitude > 0:
            retval = retval + str.format("{:.2f}",self.Longitude) + " °W"
        else:
            retval = retval + str.format("{:.2f}",-self.Longitude) + " °E"  
        return retval 


#https://www.google.com/maps/@54.3995976,18.5639144,8z
    def GetCoordsWithLink(self):
        retval = f"<a href=\"https://www.google.com/maps/@{str(self.Latitude)},{str(self.Longitude)},8z\">"
        retval = retval + self.GetCoords()
        retval = retval + "</a>"
        return retval 

    def __str__(self):
        try:
            return f"{self.Name}/{datetime.strftime(self.IssueDate,'%Y-%m-%d')}/{datetime.strftime(self.ForecastDate,'%Y-%m-%d')}/{self.MinTemp}/{self.MaxTemp}/{self.Weather}"
        except:
            return f"{self.Name}/{self.IssueDate}/{self.ForecastDate}/{self.MinTemp}/{self.MaxTemp}/{self.Weather}"