import json
import os

settings = dict()

def ReadSettings(path):
    if os.path.exists(path):
        try:
            with open(path,"r", encoding='utf-8') as file:
                src = json.load(file)
                global settings
                settings["FromAddress"] = src["FromAddress"]
                settings["ToAddresses"] = src["ToAddresses"]
                settings["Subject"] = src["Subject"]
                settings["Greetings"] = src["Greetings"]
                settings["LocalCity"] = src["LocalCity"]
                settings["SMTPServerAddress"] = src["SMTPServerAddress"]
                settings["SMTPServerPort"] = src["SMTPServerPort"]
                settings["SMTPLogin"] = src["SMTPLogin"]
                settings["SMTPPassword"] = src["SMTPPassword"]
        except:
            DefaultSettings()
    else:
        DefaultSettings()

def DefaultSettings():
    settings["FromAddress"] = "FromAddress <from@example.com>"
    settings["ToAddresses"] = list()
    settings["ToAddresses"].append("ToAddress1 <to1@example.com>")
    settings["ToAddresses"].append("ToAddress2 <to1@example.com>")
    settings["Subject"] = "Wetterbericht"
    settings["Greetings"] = "Welcome"
    settings["LocalCity"] = "Gdansk"
    settings["SMTPServerAddress"] = "smtp.example.com"
    settings["SMTPServerPort"] = 465
    settings["SMTPLogin"] = "login"
    settings["SMTPPassword"] = "password"

def WriteSettings(path):
    j = json.dumps(settings)
    with open(path,"w", encoding='utf-8') as file:
        file.write(j)