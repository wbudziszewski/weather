import requests
import json
from datetime import datetime
import pyodbc

from ForecastClass import Forecast

errorsInRow = 0

print("Connecting to the database")
conn = pyodbc.connect(r"Driver={ODBC Driver 17 for SQL Server};Server=(localdb)\MSSQLLocalDB;Data Source=(localdb)\MSSQLLocalDB;Database=weather;Integrated Security=True;Connect Timeout=5;ApplicationIntent=ReadWrite;")
dbc = conn.cursor()

print("Requesting weather data and writing to the database")
for i in range(1, 10000):
    if errorsInRow > 5: break
    data = list()
    url = "https://worldweather.wmo.int/en/json/" + str(i) + "_en.xml"
    r = requests.get(url, allow_redirects=True)
    if str(r.content).find("<!DOCTYPE html>")>-1:
        errorsInRow = errorsInRow + 1
        continue

    ret = json.loads(r.content)
    city = ret["city"]
    for forecast in city["forecast"]["forecastDay"]:
        f = Forecast()
        f.Source = url
        f.Name = city["cityName"].replace("'","")
        f.Latitude = float(city["cityLatitude"])
        f.Longitude = float(city["cityLongitude"]) 
        f.IssueDate = datetime.strptime(city["forecast"]["issueDate"], "%Y-%m-%d %H:%M:%S")
        f.ForecastDate = datetime.strptime(forecast["forecastDate"], "%Y-%m-%d")
        f.MinTemp = forecast["minTemp"]
        f.MaxTemp = forecast["maxTemp"]
        f.Weather = forecast["weather"]

        if f.MinTemp.strip()=="": f.MinTemp = f.MaxTemp

        if f.Name.strip()!="" and f.IssueDate != datetime(1900,1,1) and f.ForecastDate != datetime(1900,1,1) and f.MaxTemp.strip() != "":
            print(f"{f.Name} ({f.Latitude:0.2f},{f.Longitude:0.2f}) @{f.ForecastDate}: {f.MinTemp} - {f.MaxTemp} °C / {f.Weather} from {f.Source}")
            data.append(f)
    
    errorsInRow = 0

    for d in data:
        issueDate = d.IssueDate.date().strftime('%Y-%m-%d')
        forecastDate = d.ForecastDate.date().strftime('%Y-%m-%d')
        deleteQuery = f"DELETE FROM Forecasts WHERE Name='{d.Name}' AND ForecastDate = CONVERT(date,'{forecastDate}');"
        insertQuery = f"INSERT INTO Forecasts VALUES ('{d.Name}',{d.Latitude},{d.Longitude},'{issueDate}','{forecastDate}',{d.MinTemp},{d.MaxTemp},'{d.Weather}','{d.Source}');"
        print(deleteQuery)
        print(insertQuery)
        dbc.execute(deleteQuery)
        dbc.execute(insertQuery)
        dbc.commit()

print("Success")
conn.close()