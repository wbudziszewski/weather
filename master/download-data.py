import sys
import pyodbc
import datetime
import dateutil.relativedelta
import requests
import json
import re
import logging
import os
import time

def log(msg):
    msg = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S") +" "+msg
    print(msg)
    if log != None: logging.info(msg)

#https://d.meteostat.net/app/proxy/stations/daily?station=x&start=y&end=z

args = sys.argv[1:]
historical_mode = False # Historical mode means that weather data is read from the beginning of measurements
startingStation = None
logFile = None
for a in args:
    if a.lower() == "--historical": historical_mode = True
    s = re.search("--station=(.+)", a)
    if s and len(s.groups())>0:
        startingStation = s.group(1)
    s = re.search("--log=(.+)", a)
    if s and len(s.groups())>0:
        logFile = s.group(1)
        if os.path.exists(logFile): os.remove(logFile)
        logging.basicConfig(filename=logFile, encoding='utf-8', level=logging.DEBUG)

log("Connecting to the database...")
#conn = pyodbc.connect(r"Driver={ODBC Driver 17 for SQL Server};Server=(localdb)\MSSQLLocalDB;Data Source=(localdb)\MSSQLLocalDB;Database=weather;Integrated Security=True;Connect Timeout=5;ApplicationIntent=ReadWrite;")
conn = pyodbc.connect(driver="{ODBC Driver 17 for SQL Server}", server="(localdb)\MSSQLLocalDB", database="weather", timeout=5)
dbc = conn.cursor()
dbc.fast_executemany = True

log("Getting list of weather stations...")
selectQuery = f"SELECT ID FROM Stations;"
dbc.execute(selectQuery)
results = dbc.fetchall()
stations = list()
for r in results:
    stations.append(r[0])

log("Setting historical cutoff date...")
if historical_mode: 
    startDate = datetime.datetime(1950,1,1).date()
    endDate = datetime.datetime(datetime.datetime.now().date().year-2,12,31)
else: 
    startDate = datetime.datetime(datetime.datetime.now().date().year-1,1,1)
    endDate = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

log("Downloading weather data and uploading to the database...")
counter = -1
for s in stations:
    counter = counter + 1
    if startingStation != None:
        if s == startingStation: 
            startingStation = None
        else:
            continue
    log(f"Processing {s} station ({(100*counter/len(stations)):.1f}%)...")
    deleteParams = ""
    insertParams = list()
    dates = list()
    table = ""
    if historical_mode:
        selectQuery = f"SELECT StartDate FROM Stations WHERE ID = ?;"
        selectParams = (s,)
        dbc.execute(selectQuery, selectParams)
        results = dbc.fetchall()
        if len(results)==0 or results[0][0]==None: continue
        dates.append(max(startDate,results[0][0]))
        while dates[len(dates)-1]<endDate:
            dates.append(dates[len(dates)-1] + dateutil.relativedelta.relativedelta(years=1) - dateutil.relativedelta.relativedelta(days=1))
    else:
        dates.append(startDate)
        dates.append(endDate)
    if len(dates)<2: continue

    if historical_mode:
        table = "Archive"
    else:
        table = "Data"
        
    deleteQuery = f"DELETE FROM {table} WHERE ID=?;"
    deleteParams = (s,)
    dbc.execute(deleteQuery, deleteParams)
    dbc.commit()

    for i in range(0,len(dates)-1):
        errorCount = 0
        insertParams.clear()
        url = f"https://d.meteostat.net/app/proxy/stations/daily?station={s}&start={datetime.datetime.strftime(dates[i],'%Y-%m-%d')}&end={datetime.datetime.strftime(dates[i+1],'%Y-%m-%d')}"
        r = requests.get(url, allow_redirects=True)
        while r.content == b"error code: 1001":
            log(f"Error code 1001 encountered.")
            dates[i] = dates[i] + dateutil.relativedelta.relativedelta(days=1)
            url = f"https://d.meteostat.net/app/proxy/stations/daily?station={s}&start={datetime.datetime.strftime(dates[i],'%Y-%m-%d')}&end={datetime.datetime.strftime(dates[i+1],'%Y-%m-%d')}"
            r = requests.get(url, allow_redirects=True)
        while r.content.startswith(b'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">\n<title>400 Bad Request</title>'):
            log(f"Error code 400 (bad request) encountered.")
            dates[i] = dates[i] + dateutil.relativedelta.relativedelta(days=1)
            url = f"https://d.meteostat.net/app/proxy/stations/daily?station={s}&start={datetime.datetime.strftime(dates[i],'%Y-%m-%d')}&end={datetime.datetime.strftime(dates[i+1],'%Y-%m-%d')}"
            r = requests.get(url, allow_redirects=True)
        while r.content == b'':
            log(f"Empty page content.")
            dates[i] = dates[i] + dateutil.relativedelta.relativedelta(days=1)
            url = f"https://d.meteostat.net/app/proxy/stations/daily?station={s}&start={datetime.datetime.strftime(dates[i],'%Y-%m-%d')}&end={datetime.datetime.strftime(dates[i+1],'%Y-%m-%d')}"
            r = requests.get(url, allow_redirects=True)
        while r.content.startswith(b'<html>\n<head><title>521 Origin Down'): 
            log(f"521 origin down error.")
            dates[i] = dates[i] + dateutil.relativedelta.relativedelta(days=1)
            url = f"https://d.meteostat.net/app/proxy/stations/daily?station={s}&start={datetime.datetime.strftime(dates[i],'%Y-%m-%d')}&end={datetime.datetime.strftime(dates[i+1],'%Y-%m-%d')}"
            r = requests.get(url, allow_redirects=True)
        while r.content.startswith(b'<html>\n<head><title>503 Service Temporarily Unavailable'):
            log(f"503 service temporarily unavailable error.")
            time.sleep(1)
            dates[i] = dates[i] + dateutil.relativedelta.relativedelta(days=1)
            url = f"https://d.meteostat.net/app/proxy/stations/daily?station={s}&start={datetime.datetime.strftime(dates[i],'%Y-%m-%d')}&end={datetime.datetime.strftime(dates[i+1],'%Y-%m-%d')}"
            r = requests.get(url, allow_redirects=True)
        while r.content.startswith(b'<!DOCTYPE html'): 
            log(f"Other error.")
            time.sleep(1)
            r = requests.get(url, allow_redirects=True)
            errorCount = errorCount + 1
            if errorCount >= 10:
                dbc.close()
                quit()
        raw = json.loads(r.content)
        genDate = raw["meta"]["generated"][:10]
        for d in raw["data"]:
            if d["tmin"] == None or d["tmax"]==None: continue
            insertQuery = (f"INSERT INTO {table} VALUES (?, CONVERT(date,?), CONVERT(date,?), ?, ?, ?, ?, ?);")
            insertParams.append((s, genDate, d["date"], d["tavg"], d["tmin"], d["tmax"], d["prcp"], d["snow"]))       
        if len(insertParams)>0: 
            time.sleep(0.01)
            dbc.executemany(insertQuery, insertParams)
            dbc.commit()
        log(f"Processed {url}. {len(insertParams)} entries OK")
conn.close()
log("Success")
